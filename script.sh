#Run it as sudo bash hadoopinstall.sh

#update the packages
apt-get update -y
apt-get install ssh

#java installation
apt install default-jdk -y

#java path in bashrc                                                                         #uploading Java  envirnorment variable in local system file
cat >> ~/.bashrc << EOL
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
export PATH=$JAVA_HOME/bin:$PATH
EOL

source ~/.bashrc                                                                     #restarting the bashrc service

#hadoop installation
#wget https://archive.apache.org/dist/hadoop/core/hadoop-2.7.1/hadoop-2.7.1.tar.gz 2>&1
tar -xzvf hadoop-2.7.1.tar.gz 2>&1                                                    #unziping hadoop file
tar xzf hive.tar.gz
#tar zxvf db-derby-10.4.2.0-bin.tar.gz

#echo " envirnorment variable"
mv hadoop-2.7.1 /usr/local/hadoop                                                       #moving and rename the hadoop file
chown $USER:$USER /usr/local/hadoop
mv apache-hive-3.1.2-bin /usr/local/hive
chown $USER:$USER /usr/local/hive
mv tar /usr/local/db-derby-10.4.2.0-bin /usr/local/derby  

cat >> ~/.bashrc << EOL                                                                #updating hadoop envirnorment variable to loacl system
export HADOOP_HOME=/usr/local/hadoop
export PATH=/usr/local/hadoop/sbin:/usr/local/hadoop/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/hadoop/games:/usr/local/hadoop/bin:/usr/local/hadoop/sbin
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/jre
export PATH=$PATH:/usr/local/hadoop/bin:/usr/local/hadoop/sbin
export HADOOP_MAPRED_HOME=/usr/local/hadoop
export HADOOP_COMMON_HOME=/usr/local/hadoop
export HADOOP_HDFS_HOME=/usr/local/hadoop
export YARN_HOME=/usr/local/hadoop
export HADOOP_COMMON_LIB_NATIVE_DIR=/usr/local/hadoop/lib/native
export HADOOP_OPTS="-Djava.library.path=/usr/local/hadoop/lib"
export HIVE_HOME=/usr/local/hive
export PATH=$PATH:$HIVE_HOME/bin
export CLASSPATH=$CLASSPATH:/usr/local/hadoop/lib/*:.
export CLASSPATH=$CLASSPATH:/usr/local/hive/lib/*:.
export DERBY_HOME=/usr/local/derby
export PATH=$PATH:$DERBY_HOME/bin
export CLASSPATH=$CLASSPATH:$DERBY_HOME/lib/derby.jar:$DERBY_HOME/lib/derbytools.jar
EOL

echo "~.bashrc finished"
#mkdir /usr/local/derby/data
echo "modify hadoop-env file"

cat >> /usr/local/hadoop/etc/hadoop/hadoop-env.sh << EOL                               #adding java path in hadoop envirnorment file
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
EOL
echo "hadoop env finished"
#cat >> /usr/local/hive/bin/hive-config.sh << EOL                               #adding java path in hadoop envirnorment file
#export HADOOP_HOME=/usr/local/hadoop
#EOL

echo "hadoop version"
/usr/local/hadoop/bin/hadoop version                                               #checking hadoop version

echo "configuration started"
sed -i '/<configuration>/,/<\/configuration>/d' /usr/local/hadoop/etc/hadoop/core-site.xml                      #adding configurations in hadoop configuration file
su -c 'cat >>/usr/local/hadoop/etc/hadoop/core-site.xml <<EOL
<configuration>
<property>
<name>fs.defaultFS</name>
<value>hdfs://192.168.2.24:9000</value>
</property>
<property>
<name>hadoop.tmp.dir</name>
<value>/app/hadoop/tmp</value>
</property>
</configuration>
EOL'

sed -i '/<configuration>/,/<\/configuration>/d' /usr/local/hadoop/etc/hadoop/hdfs-site.xml
su -c 'cat >>/usr/local/hadoop/etc/hadoop/hdfs-site.xml <<EOL
<configuration>
<property>
<name>dfs.replication</name>
<value>1</value>
</property>
<property>
<name>dfs.namenode.name.dir</name>
<value>file:/usr/local/hadoop_store/hdfs/namenode</value>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>file:/usr/local/hadoop_store/hdfs/datanode</value>
</property>
</configuration>
EOL'

sed -i '/<configuration>/,/<\/configuration>/d' /usr/local/hadoop/etc/hadoop/yarn-site.xml
su -c 'cat >>/usr/local/hadoop/etc/hadoop/yarn-site.xml <<EOL
<configuration>
<property>
<name>yarn.nodemanager.aux-services</name>
<value>mapreduce_shuffle</value>
</property>
</configuration>
EOL'

cd /usr/local/hadoop/etc/hadoop/
cp mapred-site.xml.template mapred-site.xml
sed -i '/<configuration>/,/<\/configuration>/d' /usr/local/hadoop/etc/hadoop/mapred-site.xml
su -c 'cat >>/usr/local/hadoop/etc/hadoop/mapred-site.xml <<EOL
<configuration>
<property>
<name>mapreduce.framework.name</name>
<value>yarn</value>
</property>
</configuration>
EOL'

echo "4 configuration files done "
#touch /usr/local/hadoop/etc/hadoop/master                                                         
#chmod u+x /usr/local/hadoop/etc/hadoop/master
echo "editing docker file"
su -c 'cat >/usr/local/hadoop/etc/hadoop/slaves <<EOL
datanode.teesside.com 192.168.2.25
EOL'
 
#su -c 'cat >>/etc/hosts <<EOL                                                                          #adding hostname and ipaddress in local dns config file
#datanode.teesside.com 192.168.2.22
#namenode.teesside.com 192.168.2.23
#EOL'

echo "ssh key gen"
ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa                                      #generating ssh key for data security process and communication b/w two nodes(namenode, datanode)
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
chmod 600 ~/.ssh/authorized_keys
ls -a ~/.ssh
echo "ssh master Key"
sleep 10
touch /usr/local/hadoop/etc/hadoop/masters

su -c 'cat >/usr/local/hadoop/etc/hadoop/masters <<EOL                      #adding master=namenode so updating namnode hostname and ipaddress in datanode to tell them thiss is namenode
namenode.teesside.com 192.168.2.24
EOL'

echo "data store creatation"

sleep 5
mkdir -p /app/hadoop/tmp
chown $USER:$USER /app/hadoop/tmp
mkdir -p /usr/local/hadoop_store/hdfs/namenode
mkdir -p /usr/local/hadoop_store/hdfs/datanode
chown $USER:$USER /usr/local/hadoop_store/hdfs/namenode
chown $USER:$USER /usr/local/hadoop_store/hdfs/datanode


#__________________________________ testing below commands add this stage-------------------------------------------------------------------

/etc/init.d/ssh restart                                              #restarting ssh
#ssh-copy-id -i $HOME/.ssh/id_rsa.pub root@192.168.2.22             #ssh key-copy working on it
#ssh localhost

#hadoop version
#/usr/local/bin/hdfs namenode -format                             #formating the namenode to clear data inside the namenode for safeside
#/usr/local/sbin/start-dfs.sh                                    #starting  services or daemons of hadoop 
#jps                                                              #checking how many daemons are up and running 

