From ubuntu:16.04

MAINTAINER swarupa katrala <swarupa.katrala123@gmail.com>
RUN apt-get update -y                                      
RUN apt-get install openjdk-8-jdk -y                       
RUN apt-get install -y wget                                
RUN apt-get install -y openssh-server                      
#RUN chmod u+x /user/local
#RUN /opt/ ls -la
#RUN mkdir -p /scripts
#COPY ./script.sh /scripts
#WORKDIR /scripts
#RUN chmod +x script.sh
#RUN ls -la
#RUN bash ./script.sh
#RUN mkdir -p /opt/ha
COPY ./hadoop-2.7.1.tar.gz /usr/local                           
COPY ./script.sh /usr/local                                     
COPY ./ssh /usr/local                                            
COPY ./hive.tar.gz /usr/local
COPY ./db-derby-10.4.2.0-bin.tar.gz /usr/local
#RUN chown root:root /user/local/hadoop
WORKDIR /usr/local                                              
RUN chmod +x script.sh                                           
RUN ls -la                                                      
RUN bash ./script.sh
RUN mv /usr/local/ssh ~/.ssh/config                             

#RUN echo "namenode.teesside.com" >> /root/.ssh/config 
#RUN echo " User root" >> /root/.ssh/config

#CMD ["/etc/bootstrap.sh", "-d"]

# Hdfs ports
EXPOSE 50010 50020 50070 50075 50090 8020 9000      
# Mapred ports
EXPOSE 10020 19888
#Yarn ports
EXPOSE 8030 8031 8032 8033 8040 8042 8088
#Other ports
EXPOSE 49707 2122

EXPOSE 9000

EXPOSE 2022
EXPOSE 1527
#CMD [ "sh", "/etc/init.d/ssh", "start"]              
