#use as bash script.sh 24 namenode 25 datanode 
docker build -t hadoop_1.0 .
echo "hadoop_1.0 image built successfully"
sleep 5;
docker run --net hadoop_net --ip 192.168.2.$1 -h $2.teesside.com --name $2 --memory=3GB -it hadoop_1.0 bash
echo "hadoop_1.0 namenode is built successfully now starting it"
docker run --net hadoop_net --ip 192.168.2.$3 -h $4.teesside.com --name $4 -it hadoop_1.0 bash
echo "hadoop_1.0 datanode built successfully"
docker start  $2 $4
sleep 20
echo "hadoop_1.0 image built successfully"
echo "ssh is restarting"

docker exec -ti $2 bash -c "/etc/init.d/ssh restart"
docker exec -ti $4 bash -c "/etc/init.d/ssh restart"
docker exec -ti $4 bash -c "/etc/init.d/ssh restart"

sleep 20;

docker exec -ti $2 bash -c "sh -c 'echo 192.168.2.25   datanode.teesside.com >> /etc/hosts'"
docker exec -ti $4 bash -c "sh -c 'echo 192.168.2.24   namenode.teesside.com >> /etc/hosts'"

docker exec -ti $4 bash -c "rm -rf /usr/local/hadoop/etc/hadoop/master"

docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs $2 -format"
echo "namenode format successfully"

docker exec -ti $2 bash  -c "/usr/local/hadoop/sbin/start-dfs.sh && /usr/local/hadoop/sbin/start-yarn.sh"

echo "services starting now successfully"
docker exec -ti $2 bash -c "jps"
docker exec -ti $4 bash -c "jps"

docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -mkdir -p input"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -ls -R /"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hadoop fs -put /usr/local/hadoop/etc/hadoop/*.xml input"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hadoop jar /usr/local/hadoop/share/hadoop/mapreduce/hadoop-mapreduce-examples-2.7.1.jar grep input output 'dfs[a-z.]+'"
echo "Hive configuration"

docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -mkdir /tmp"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -chmod g+w /tmp"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -ls /"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -mkdir -p /user/hive/warehouse"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -chmod g+w /user/hive/warehouse"
docker exec -ti $2 bash -c "/usr/local/hadoop/bin/hdfs dfs -ls /user/hive"
docker exec -ti $2 bash -c "cp /usr/local/hadoop/share/hadoop/hdfs/lib/guava-11.0.2.jar /usr/local/hadoop/hive/lib/"
docker exec -ti $2 bash -c "/usr/local/hive/bin/schematool -–initSchema –dbType derby"
