#Run it as sudo bash hadoopinstall.sh

#update the packages
apt-get update -y
apt-get install ssh default-jdk -y
cat >> ~/.bashrc << EOL
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
export PATH=$JAVA_HOME/bin:$PATH
EOL

source ~/.bashrc
tar -xzvf hadoop-2.7.1.tar.gz 2>&1
tar xzf hive.tar.gz
tar zxvf db-derby-10.4.2.0-bin.tar.gz

#echo " envirnorment variable"
mv hadoop-2.7.1 hadoop
chown $USER:$USER hadoop
mv apache-hive-3.1.2-bin hive
chown $USER:$USER hive
mv db-derby-10.4.2.0-bin derby

echo "~.bashrc finished"
#mkdir /usr/local/derby/data
echo "modify hadoop-env file"

cat >> /usr/local/hadoop/etc/hadoop/hadoop-env.sh << EOL
export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/bin/java::")
EOL

echo "hadoop version"
echo "configuration started"

echo "4 configuration files done "

echo "editing docker file"
cd hadoop/etc/hadoop
rm core-site.xml hdfs-site.xml yarn-site.xml
cd /usr/local
mv core hadoop/etc/hadoop/core-site.xml
mv hdfs hadoop/etc/hadoop/hdfs-site.xml
mv yarn hadoop/etc/hadoop/yarn-site.xml
mv map hadoop/etc/hadoop/mapred-site.xml


echo "ssh key gen"
ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa
sleep 10

touch hadoop/etc/hadoop/masters

su -c 'cat >hadoop/etc/hadoop/masters <<EOL
namenode.teesside.com 192.168.2.24
EOL'

su -c 'cat >hadoop/etc/hadoop/slaves <<EOL
datanode.teesside.com 192.168.2.25
EOL'

echo "data store creation"

sleep 5
mkdir -p /app/hadoop/tmp
chown $USER:$USER /app/hadoop/tmp
mkdir -p hadoop_store/hdfs/namenode
mkdir -p hadoop_store/hdfs/datanode
chown $USER:$USER hadoop_store

